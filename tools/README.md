# Tools


## Discourse parsers

### Summary


|  tool  | task      | date |         authors    | Easy2use |
|--------|-----------|------|--------------------|----------|
|Codra P.|RST parsing|2015  | Shafiq Joty et  al |    yes   |
|NNDis P.|RST parsing|2018  | Nan Yu et       al |    yes   |
|Multi P.|RST parsing|2016  | Chloe Braud et  al |          |
|Sh-Re P.|RST parsing|2014  | Yangfeng Ji et  al |          |
|Pdtb P. |PDTB parsin|2010  | Ziheng Lin         |    yes   |


### Links
- Codra :  https://github.com/NLPbox/codra-docker
- NNdis : https://github.com/yunan4nlp/NNDisParser
- pdtb  : https://github.com/linziheng/pdtb-parser

### Other ressources

- Tatjana Scheffler @ ESSLLI 2019  :  https://github.com/TScheffler/2019ESSLLI-discparsing/tree/master/day3


## Sent. similarity (embeddings)

- Argumentation clustering and classification : https://github.com/UKPLab/acl2019-BERT-argument-classification-and-clustering/blob/master/argument-similarity/README.md

## Data mining



|  Algo  | task      | date | authors |  implementation  |
|--------|-----------|------|--------------------|----------|
|**gSpan**|frequent subgraph mining| 2002 | Yan et Han |    https://github.com/betterenvi/gSpan   |
|**ReRemi**|Redescription mining| 2018 | Galbrun |   Siren tool |
|**ZART**|Association rule mining | 2004 |  | AssRuleX (Coron)|
|CMTreeMiner | Closed and maximal frequent subtrees mining|2004|Chi and Yang | https://github.com/search?q=cmtreeminer|
