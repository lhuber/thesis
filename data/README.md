# Data

## Annotated corpus

### Summary


|  dataset  | annot      | genre | rk | # docs | source | date |
|--------|-----------|------|--------------------|----------|------|----|
| **SciDTB** | RST - dep | scientific abstracts | RST modified | 798 | https://www.aclweb.org/anthology/P18-2071/ | 2018 |
| **RST-DT** | RST - classic | newspapers (wsj) |  | 385 | Marcu (https://www.isi.edu/~marcu/discourse/) |
| **SFU-reviews** | RST - sent. level | opinion+ description | 8 cat. | 400 | Taboada (https://www.sfu.ca/~mtaboada/SFU_Review_Corpus.html) |
| **Arg Micro texts** | Arg + RST + SDRT | argumentatif (full) | sev. contro. quest | 112 (all annots) + 171 | https://github.com/peldszus/arg-microtexts | 2015|
| **GUM (NEW!!!2020)** | RST + others | news+ howto + guides | |148 | Amir Zeldes (http://corpling.uis.georgetown.edu/gum/)| 2017|
|**BioDRB**|PDTB| Biomedical articles | |24| Prasad (https://www.ncbi.nlm.nih.gov/pubmed/21605399) | 2011 |
|**PDTB**|PDTB| newspapers (wsj)|| 2230 |Aravind Joshi (https://www.seas.upenn.edu/~pdtb/)| 2008|

### Details

#### GUM
(update 06/03/2020)
- 22 documents added
- 5 new dicourse relations  
- format follows now RST DT guideline

## Representation of units (embeddings)

### Models





### Dataset
